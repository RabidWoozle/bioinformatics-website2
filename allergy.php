<!DOCTYPE html>
<?php

session_start();
require_once('./php/userDB.php');


if(!isset($_SESSION['userID'])){

    header('Location: index.php');
    die();
}


?>
<html lang="en">
<head>
    <?php
        include("./php/headerLinks.php");
        ?>
   <script>
    $(document).ready(function() {
    $('#allergyViewTable').DataTable();
} );
    </script>
    
</head>

<body>

    <?php include("./php/navbar.php"); ?>

<!--End of Nav Bar -->
    
    
<div id="about" class="main-content container">

    <div  class="main-content row " >
        <div class="col-md-12">
            <div class="row sidebar-profile">
                
            <table id="allergyViewTable" class="display" cellspacing="0" width="100%">
                <?php
                    include("./php/allergyFunctions.php");
                ?>
            </table>
            </div>
        
        </div>
        
        
    </div>
    
</div><!-- /.container -->


    <footer class="page-footer text-center">
        <div class="container-fluid">
            <div class="row"><p>This tool was created for Bioinformatics at Oakland University Fall-2015</p></div>
        </div>
    </footer>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
     
    <script src="./js/jquery-1.11.3.min.js"></script>
    <script src="./js/jquery.dataTables.min.js"></script>
    
   <!-- <script>
   $(document).ready(function() {
    $('#allergyViewTable').DataTable();
} );
    </script>
-->
</body>
</html>
