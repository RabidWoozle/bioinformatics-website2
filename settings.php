<!DOCTYPE html>
<?php 

session_start();

if(!isset($_SESSION['userID'])){
    
    header('Location: index.php');
    die();
}

//include('/php/editProfile.php');
$maleRadio = "";
$maleChecked ="";
$femaleRadio = "";
$femaleChecked = "";
if($_SESSION['userSex'] == "Male"){
    $maleRadio = "active";
    $maleChecked = 'checked=\"\"';
}
else{
    $femaleRadio = "active";
    $femaleChecked = 'checked=\"\"';
}
?>
<html lang="en">
  <head>
    
    <?php include("./php/headerLinks.php"); ?>
      
    <link href="./css/settings.css" rel="stylesheet">
      
    <link href="./css/bootstrap-datepicker.standalone.css" rel="stylesheet" />
    <script src="./js/bootstrap-datepicker.min.js"></script>
    
    <script>
    $('#userAge').datepicker({

})
    </script>
    
  </head>

  <body>

    <?php include("./php/navbar.php"); ?>

    <div id="about" class="main-content container">

      <div  class="main-content row profile-side" >
          <div class="col-sm-2 col-md-3 col-md-offset-1 sidebar">
          <div class="row sidebar-profile">
              <div class="col-md-12">
                  <img src="img/profile.png" class="img-circle">
             <h4 class="card-title">Welcome </h4>
                  <h5 class="card-subtitle"><?php echo $_SESSION['userName'] . " " . $_SESSION['userLastName']; ?> </h5>
        
              
              
              <div class="row">
                  <ul class="list-group list-group-flush">
                  <li class="list-group-item">
                      <a href="profile.php">Profile</a>
                      </li>
                  </ul>
                  
              </div>
          </div>
      </div>
          </div>
        <div class="col-sm-2 col-md-6 col-md-offset-1 sidebar">
          <div class="row sidebar-profile">
              <div class="col-md-12">
                  
             <h4 class="card-title">Settings </h4>
                  
              <div class="row">
                 <div class="col-md-12">
                  <form role="form" method="post" action="./php/editProfile.php">
                     <div class="form-group">
                      <label for="firstname">First Name:</label>
                         <input class="form-control" type="text" id="firstname" name="firstname" placeholder="First Name" value="<?php echo $_SESSION['userName'] ?>" required>
                      </div>
                      <div class="form-group">
                      <label for="lastname">Last Name:</label>
                         <input class="form-control" type="text" id="lastname" name="lastname" placeholder="Last Name" value="<?php echo $_SESSION['userLastName'] ?>" required>
                      </div>
                     <div class="form-group">
                      <label for="userAge">Date of Birth:</label>
                         <input type="text" id="userAge" name="userAge" class="form-control" placeholder="mm/dd/yyyy" data-provide="datepicker" value="<?php echo $_SESSION['userDateOfBirth'] ?>" required>
                      </div>
                      <div class="form-group">
                      <label for="zip">Zip Code:</label>
                         <input class="form-control" type="text" id="zip" name="zip" placeholder="ZIP" value="<?php echo $_SESSION['userZip'] ?>" required>
                      </div>
                       <div class="form-group">
                      <label for="zip">Sex:</label>
                         <label class="radio-inline <?php echo $maleRadio; ?>"><input type="radio" name="radioOption" value="Male" <?php echo $maleChecked; ?> >Male</label>
                           <label class="radio-inline <?php echo $femaleRadio; ?>"><input type="radio" name="radioOption" value="Female" <?php echo $femaleChecked; ?>>Female</label>
                      </div>
                      <div class=" pull-right">
                      <button class="btn btn-success settings-btn"  type="submit" name="SaveButton">Save</button>
                          <button class="btn btn-default settings-btn" type="reset">Cancel</button>
                      </div>
                     </form>
                  
                  </div>
                  
              </div>
          </div>
      </div>
          </div>
        </div>

    </div><!-- /.container -->
      
      
          <footer class="page-footer text-center">
              <div class="container-fluid">
                  <div class="row"><p>This tool was created for Bioinformatics at Oakland University Fall-2015</p></div>
              </div>
      </footer>

  </body>
</html>
