<!DOCTYPE html>

<?php

session_start();
require('./php/userDB.php');
include('./php/adminFunctions.php');

if(!isset($_SESSION['userID']) || !isset($_SESSION['admin']) || $_SESSION['admin'] != true)
{

    header('Location: index.php');
    die();
}
?>

<html lang="en">
<head>
    <?php
        include("./php/headerLinks.php");
        ?>
    <script>
   $(document).ready(function() {
    $('table.display').DataTable();
} );
    </script>
</head>

<body>

    <?php include("./php/navbar.php"); ?>

<!--End of Nav Bar -->
    
    
<div id="about" class="main-content container">

    <div  class="row " >
        <div class="col-md-12">
           <h3>Food</h3>
            
            <?php echo loadFoodTable(); ?>
            
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <h3>Food Type</h3>
            
            <?php echo loadFoodTypeTable(); ?>
        </div>
        <div class="col-md-6">
             <h3>Allergen</h3>
            <?php echo loadAllergenTable(); ?>
        </div>
    </div>
    
    <hr>
    
</div><!-- /.container -->


    <footer class="page-footer text-center">
        <div class="container-fluid">
            <div class="row"><p>This tool was created for Bioinformatics at Oakland University Fall-2015</p></div>
        </div>
    </footer>


-->
</body>
</html>
