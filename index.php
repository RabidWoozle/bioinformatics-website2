<!DOCTYPE html>

<?php 
    session_start(); 

?>

<html lang="en">

<head>
     <?php
        include("./php/headerLinks.php");
       include('./php/chartsAndGraphs.php');
                        echo top5FoodAllergiesChart();
                        echo percentageOfAgeGroupAndTheirTop5Allergies();
        ?>
    <link href="./css/bootstrap-datepicker.standalone.css" rel="stylesheet" />
    <script src="./js/bootstrap-datepicker.min.js"></script>
    
    <script>
    $('#input-age-signup').datepicker({
    startView: 2
})
    </script>
    
    <style>
        #input-zip-signup {
            margin-bottom: 5px;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

     <?php include("./php/navbar.php"); ?>

    <div class="modal fade" id="signup" data-target="#signup">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><strong></strong>Sign Up</h4>


                </div>
                
                <!--Start of Registration Modal-->
                
                <div class="modal-body">
                    <form name="signup" action="./php/registration.php" method="post" role="signup">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="input-email-signup">Email:</label>
                                <input name="email" type="email" id="input-email-signup" class="form-control" placeholder="E-Mail" required>
                            </div>
                            <div class="col-xs-6">
                                <label for="input-firstName-signup">First Name:</label>
                                <input name="fname" type="text" id="input-firstName-signup" class="form-control" placeholder="First Name" required>
                            </div>

                            <div class="col-xs-6">
                                <label for="input-lastName-signup">Last Name:</label>
                                <input name="lname" type="text" id="input-lastName-signup" class="form-control" placeholder="Last Name" required>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="input-password-signup">Password:</label>
                                <input name="password" type="password" id="input-password-signup" class="form-control" placeholder="Password" required>
                            </div>

                            <div class="col-xs-12">
                                <label for="input-password2-signup">Confirm Password:</label>
                                <input name="password2" type="password" id="input-password2-signup" class="form-control" placeholder="Retype Password" required>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="input-age-signup">Date of Birth:</label>
                                <input name="age" type="text" id="input-age-signup" class="form-control" placeholder="mm/dd/yyyy" data-provide="datepicker" required>
                            </div>
                            <div class="col-xs-6">
                                <label for="input-zip-signup">Zip Code:</label>
                                <input name="zip" type="text" id="input-zip-signup" class="form-control" placeholder="zip" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="radioOption">Sex:</label>
                                <label class="radio-inline">
                                    <input type="radio" name="radioOption" value="Male">Male</label>
                                <label class="radio-inline">
                                    <input type="radio" name="radioOption" value="Female">Female</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn  btn-default">Sign Up</button>
                            <button type="reset" class="btn  btn-danger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cancel</span></button>

                        </div>
                        <!--                                                                <button class="btn btn-default" type="submit">Login</button>-->


                    </form>
                </div>
                <!--<div class="modal-footer"></div>-->

            </div>
        </div>
    </div>

    <div id="about" class="main-content container">

        <div class="main-content row">
            <!--<h1>Welcome To AllerGEN</h1>
          <p class="lead">AllerGEN is a tool that allows users to build a personalized allergen profile. This profile assists with allergy risk assessment and provides alternative food options based on your personal reaction to certain allergens.</p> -->

            <div id="info-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#info-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#info-carousel" data-slide-to="1"></li>
                    <li data-target="#info-carousel" data-slide-to="2"></li>

                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="img/AllerGENBanner.png" alt="banner1">
                        <div class="carousel-caption">
                            <h1>Welcome To AllerGEN</h1>
                            <p class="lead">AllerGEN is a tool that allows users to build a personalized allergen profile. This profile assists with allergy risk assessment and provides alternative food options based on your personal reaction to certain allergens.</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/MakeProfileBanner.png" alt="banner2">
                        <div class="carousel-caption">
                            <h1>Make A Profile</h1>
                            <p class="lead">AllerGEN allows for users to create personalized profiles using biometric and location data. Creating a unique risk assessment and report based on factors including Age, Sex, and Location.</p>
                        </div>
                    </div>
                    <div class="item ">
                        <img src="img/FindSolutionBanner.png" alt="banner3">
                        <div class="carousel-caption">
                            <h1>Find Solutions</h1>
                            <p class="lead">AllerGEN view live reports and discover solutions to their specific allergen sensitivites.</p>
                        </div>
                    </div>

                </div>

                <a class="left carousel-control" href="#info-carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#info-carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div id="top5FoodAllergiesChart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                
                <div class="col-md-6">
                    <div id="breakDownChart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->


    <footer class="page-footer text-center">
        <div class="container-fluid">
            <div class="row">
                <p>This tool was created for Bioinformatics at Oakland University Fall-2015</p>
            </div>
        </div>
    </footer>

</body>

</html>