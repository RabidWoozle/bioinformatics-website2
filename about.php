<!DOCTYPE html>

<?php
session_start();
require('./php/userDB.php');
?>

<html lang="en">
<head>
    <?php
        include("./php/headerLinks.php");
        ?>
    
</head>

<body>

    <?php include("./php/navbar.php"); ?>

<!--End of Nav Bar -->
    
    
<div id="about" class="main-content container">

    <div  class="main-content row " >
        <div class="col-md-12 sidebar">
           <h2>About AllerGEN</h2>
            <hr>
            
            <h3>What is AllerGEN?</h3>
            <p>AllerGEN allows users with allergies to create a profile and fill out their allergy list. From their chosen allergies, AllerGEN does the work for the user to determine what foods they should avoid.</p>
            
            <h3>How accurate are these results?</h3>
            <p>There is still a lot of research to be done on allergies and how cross reactivity affects different people. Always consult with your physician if you have any concerns about your allergies or would like to change your diet.</p>
            <p>The information that AllerGEN provides are suggestions and will not always apply to everyone. With an allergy and with manufacturing of foods causing increased cross contamination you will need to stay alert to the foods you eat so that you avoid any allergic reactions.</p>
            
            <h3>How does AllerGEN relate to Bioinformatics?</h3>
            <p>Bioinformatics combines elements of biology and computer science. AllerGEN complements both of these fields as we have developed a web application that presents biological information related to allergies and their genetic sequences to users.</p>
            
            <hr>
            
            <h4>Resources</h4>
            <ul>
                <li><a href="http://acaai.org/allergies">Allergies | Symptoms and Treatment</a></li>
            </ul>
            
            
            <h4>Scientific Papers</h4>
            
            <ul> 
                <li><a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4620636/"> Tropomyosin, the Major Tropical Oyster Crassostrea Belcheri Allergen and Effect of Cooking on its Allergenicity</a></li>
            <li><a href="http://toxsci.oxfordjournals.org/content/55/2/235.full">Why are some Proteins Allergens?</a></li>
            
            </ul>
           
            
        </div>
        
        
    </div>
    
</div><!-- /.container -->


    <footer class="page-footer text-center">
        <div class="container-fluid">
            <div class="row"><p>This tool was created for Bioinformatics at Oakland University Fall-2015</p></div>
        </div>
    </footer>

</body>
</html>
