<?php 
/*
List of Functions for the allergies listed in database

*/



if(isset($_POST['user_add_allergy_btn']))
{     
    SaveAddedAllergyToUserAccount();
}

if(isset($_POST['user_delete_allergy_btn']))
{
    DeleteAllergyFromUserAccount();
}
LoadAllergiesToAdd();

function LoadAllergiesToView()
{
}

function LoadAllergiesToAdd()
{
    //session_start();
    require_once('./php/userDB.php');
    
    $database = new CustomDatabase();
    $db_conn = $database->connect_to_db();
    $uid = $_SESSION['userID'];
    
    echo "<thead>
            <tr>
                <th>Allergy</th>
                <th>Type</th>
                <th>Protein</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
               <th>Allergy</th>
                <th>Type</th>
                <th>Protein</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>";
    
        $sql = $db_conn->prepare("SELECT  foo.food_id,
                        foo.food_name,
                        ft.food_type_name,
                        group_concat(al.allergen_name separator ', ') AS allergen,
                        CASE WHEN EXISTS(SELECT algy.food_id FROM user_allergy algy WHERE algy.food_id = foo.food_id AND algy.user_id = ?)
                        	THEN 1 ELSE 0 END AS userAllergenFound
                FROM food foo
                LEFT JOIN food_type ft ON foo.food_type_id = ft.food_type_id
                LEFT JOIN food_allergen fa ON foo.food_id = fa.food_id
                LEFT JOIN allergen al ON al.allergen_id = fa.allergen_id
                GROUP BY foo.food_id, foo.food_name, ft.food_type_name");
        $sql->bind_param('s', $uid);
        $sql->execute();
   
    $result = $sql->get_result();
        if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            {               
                    echo "
                            <tr>
                          <td >" . $row['food_name'] . "</td>
                          <td >" . $row['food_type_name'] . "</td>
                          <td>". $row['allergen'] . "</td>
                          <td>";
                
                //If == to 0 then the user had not added the allergy to their list of allergies
                //and we want to show them the button to add it.
                if($row['userAllergenFound'] == 0) 
                {
                    echo "<form method='post' action=''>
                            <button name='user_add_allergy_btn' type='submit' class='btn btn-default btn-sm'> 
                                Add 
                            </button>
                            <input type='hidden' name='food_id' value =" . $row['food_id'] . " />
                             </form>";
                }
                //Else, if == 1, then the user has this allergy in their file. Give them the ability to delete it
                if($row['userAllergenFound'] == 1)
                {
                    echo "<form method='post' action=''>
                            <button name='user_delete_allergy_btn' type='submit' class='btn btn-danger btn-sm'> 
                                Remove
                            </button>
                            <input type='hidden' name='food_id' value =" . $row['food_id'] . " />
                             </form>";
                }
                    echo " </td>
                          
                          </tr> ";    
            }
        }
        
        $sql->close();
                 
   echo "</tbody>";
   
}

function SaveAddedAllergyToUserAccount()
{
    //session_start();
    require_once('./php/userDB.php');
    $db = new CustomDatabase();
    $db_conn = $db->connect_to_db();
    
    
    $foodID = $_POST['food_id'];
    $userID = $_SESSION['userID']; 
    
    $sql = $db_conn->prepare(" INSERT INTO user_allergy(user_id, food_id) 
             VALUES ( ?, ?)");
    $sql->bind_param('ss', $userID, $foodID);
    $sql->execute();
    
    $result = $sql->get_result();
  
}

function DeleteAllergyFromUserAccount()
{
    require_once('./php/userDB.php');
    $db = new CustomDatabase();
    $db_conn = $db->connect_to_db();
    
    $foodID = $_POST['food_id'];
    $userID = $_SESSION['userID'];
    
    $sql = $db_conn->prepare("DELETE FROM user_allergy
            WHERE food_id = ? AND user_id = ?");
    
    $sql->bind_param('ss', $foodID, $userID);
    
    $sql->execute();
    
    
    $result = $sql->get_result();
}


?>