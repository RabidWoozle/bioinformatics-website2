<?php

function top5FoodAllergiesChart()
{
    require_once('./php/userDB.php');

    $userConn = new CustomDatabase();
    $chartConn = $userConn->connect_to_db();
    $sql = "SELECT 	foo.food_name,
	   	COUNT(foo.food_id) AS totalFood
FROM user_allergy ua
LEFT JOIN food foo ON ua.food_id = foo.food_id
GROUP BY foo.food_name
ORDER BY totalFood DESC, foo.food_name DESC
LIMIT 5";

    echo "<script> 
    $(function () {
    $('#top5FoodAllergiesChart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Top 5 Food Allergies on AllerGEN'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Foods',
            colorByPoint: true,
            data: [";
    
    $result = $chartConn->query($sql) or die($chartConn->error);
    //Need a counter so that an extra comma isn't added.
    $counter = mysqli_num_rows($result) - 1;

        if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            { 
                if($counter == 0)
                {
                     echo " { 
                            name: '" . $row['food_name'] . "',
                            y: " . $row['totalFood'] . ",
                            sliced: true
                            }";
                }
                else
                {
                    echo " { 
                            name: '" . $row['food_name'] . "',
                            y: " . $row['totalFood'] . ",
                            sliced: true
                            },";
                    $counter--;
                }
            }
        }
    echo "]
        }]
    });
});
</script>";
}

function percentageOfAgeGroupAndTheirTop5Allergies()
{
    require_once('./php/userDB.php');
    $userConn = new CustomDatabase();
    $chartConn = $userConn->connect_to_db();
    $sql = "SELECT COUNT(*) AS totalInGroup,
		CASE 
        	WHEN sq.age BETWEEN 0 AND 17 THEN '0-17'
            WHEN sq.age BETWEEN 18 AND 25 THEN '18-25'
            WHEN sq.age BETWEEN 26 AND 45 THEN '26-45'
            WHEN sq.age BETWEEN 45 AND 55 THEN '46-55'
            WHEN sq.age BETWEEN 56 AND 70 THEN '56-70'
            WHEN sq.age BETWEEN 71 AND 150 THEN '70-immortal'
        END AS ageband

FROM
	(SELECT TIMESTAMPDIFF(YEAR, STR_TO_DATE(us.user_date_of_birth, '%m/%d/%Y'), CURDATE()) AS age,
          	us.user_id,
     		al.allergen_name
    FROM user us
    LEFT JOIN user_allergy ua ON us.user_id = ua.food_id
    LEFT JOIN food foo ON ua.food_id = foo.food_id
    LEFT JOIN food_allergen fa ON foo.food_id = fa.food_id
    LEFT JOIN allergen al ON fa.allergen_id = al.allergen_id
GROUP BY us.user_id, al.allergen_name, age
 ) AS sq
GROUP BY ageband
ORDER BY ageband;";

    /*$sqlSubReport = "SELECT COUNT(*) AS totalInGroup,
		sq.allergen_name,
		CASE 
        	WHEN sq.age BETWEEN 0 AND 17 THEN '0-17'
            WHEN sq.age BETWEEN 18 AND 25 THEN '18-25'
            WHEN sq.age BETWEEN 26 AND 45 THEN '26-45'
            WHEN sq.age BETWEEN 45 AND 55 THEN '46-55'
            WHEN sq.age BETWEEN 56 AND 70 THEN '56-70'
            WHEN sq.age BETWEEN 71 AND 150 THEN '70-immortal'
        END AS ageband

FROM
	(SELECT TIMESTAMPDIFF(YEAR, STR_TO_DATE(us.user_date_of_birth, '%m/%d/%Y'), CURDATE()) AS age,
          	us.user_id,
     		al.allergen_name
    FROM user us
    LEFT JOIN user_allergy ua ON us.user_id = ua.food_id
    LEFT JOIN food foo ON ua.food_id = foo.food_id
    LEFT JOIN food_allergen fa ON foo.food_id = fa.food_id
    LEFT JOIN allergen al ON fa.allergen_id = al.allergen_id
GROUP BY us.user_id, al.allergen_name, age
 ) AS sq
GROUP BY ageband, sq.allergen_name
ORDER BY ageband, totalInGroup DESC";*/
    
     //Gets age groups and numbers
    $resultAge = $chartConn->query($sql) or die($chartConn->error);
    //Need a counter so that an extra comma isn't added.
    $counter = mysqli_num_rows($resultAge) - 1;
    

    echo "<script>
    
        $(function () {

    var colors = Highcharts.getOptions().colors,
        categories = ['0-17', '18-25', '26-45', '46-55', '56-70', '70-immortal'],
        data = [";
    
   
    
    //Need to track the allergen count. We only want to show the top five.
    $aCounter = 0;

    if($resultAge)
        {
            while($row = mysqli_fetch_assoc($resultAge))
            {               
                
                if($counter == 0)
                { 
                    echo "{y: " . $row['totalInGroup'] . ",
                          color: colors[" . $counter . "],
                          drilldown: {
                                name: '" . $row['ageband'] . "',
                                categories: [";
                    
                    //Get the sub report for the allergen numbers with age group.
                    $sqlSubReport = "SELECT * FROM 
                   ( SELECT COUNT(*) AS totalInGroup,
		sq.allergen_name,
		CASE 
        	WHEN sq.age BETWEEN 0 AND 17 THEN '0-17'
            WHEN sq.age BETWEEN 18 AND 25 THEN '18-25'
            WHEN sq.age BETWEEN 26 AND 45 THEN '26-45'
            WHEN sq.age BETWEEN 45 AND 55 THEN '46-55'
            WHEN sq.age BETWEEN 56 AND 70 THEN '56-70'
            WHEN sq.age BETWEEN 71 AND 150 THEN '70-immortal'
        END AS ageband

FROM
	(SELECT TIMESTAMPDIFF(YEAR, STR_TO_DATE(us.user_date_of_birth, '%m/%d/%Y'), CURDATE()) AS age,
          	us.user_id,
     		al.allergen_name
    FROM user us
    LEFT JOIN user_allergy ua ON us.user_id = ua.food_id
    LEFT JOIN food foo ON ua.food_id = foo.food_id
    LEFT JOIN food_allergen fa ON foo.food_id = fa.food_id
    LEFT JOIN allergen al ON fa.allergen_id = al.allergen_id
GROUP BY us.user_id, al.allergen_name, age
 ) AS sq 
GROUP BY ageband, sq.allergen_name
ORDER BY ageband, totalInGroup DESC) AS a 
 WHERE ageband = '" . $row['ageband'] . "';";
                    
                    $resultSubReport = $userConn->query($sqlSubReport) or die($userConn->error);
                    
                    $total = count($resultSubReport - 1);
                    
                    
                    if($resultSubReport)
                    {   
                        while($rowAll = mysqli_fetch_assoc($resultSubReport))
                         {                        
                             if($total == 0)
                                    {
                                        echo " '" . $rowAll['allergen_name'] . "' ";
                                    }
                                    else
                                    {
                                        echo " '" . $rowAll['allergen_name'] . "' ";
                                    }
                              $total--;
                                
                             
                         }
                    }
                    
                    echo       "],
                                data: [";
                     $resultSubReport = $userConn->query($sqlSubReport) or die($userConn->error);
                    $total2 = count($resultSubReport - 1);
                    if($resultSubReport)
                    {  
                            while($rowAll = mysqli_fetch_assoc($resultSubReport))
                         { 
                             if($total2 == 0)
                             {
                                 echo " " . $rowAll['totalInGroup'] . "";
                             }
                             else
                             {
                                 echo "" . $rowAll['totalInGroup'] . "";
                             }
                             
                             $total2--;
                            }
                    }
                    
                    echo                "],
                                color: colors[" . $counter . "]
                                    }
                                 }";
                }
                else
                {
                     echo "{y: " . $row['totalInGroup'] . ",
                          color: colors[" . $counter . "],
                          drilldown: {
                                name: '" . $row['ageband'] . "',
                                categories: [";
                    
                    //Get the sub report for the allergen numbers with age group.
                    $sqlSubReport = "SELECT * FROM 
                   ( SELECT COUNT(*) AS totalInGroup,
		sq.allergen_name,
		CASE 
        	WHEN sq.age BETWEEN 0 AND 17 THEN '0-17'
            WHEN sq.age BETWEEN 18 AND 25 THEN '18-25'
            WHEN sq.age BETWEEN 26 AND 45 THEN '26-45'
            WHEN sq.age BETWEEN 45 AND 55 THEN '46-55'
            WHEN sq.age BETWEEN 56 AND 70 THEN '56-70'
            WHEN sq.age BETWEEN 71 AND 150 THEN '70-immortal'
        END AS ageband

FROM
	(SELECT TIMESTAMPDIFF(YEAR, STR_TO_DATE(us.user_date_of_birth, '%m/%d/%Y'), CURDATE()) AS age,
          	us.user_id,
     		al.allergen_name
    FROM user us
    LEFT JOIN user_allergy ua ON us.user_id = ua.food_id
    LEFT JOIN food foo ON ua.food_id = foo.food_id
    LEFT JOIN food_allergen fa ON foo.food_id = fa.food_id
    LEFT JOIN allergen al ON fa.allergen_id = al.allergen_id
GROUP BY us.user_id, al.allergen_name, age
 ) AS sq 
GROUP BY ageband, sq.allergen_name
ORDER BY ageband, totalInGroup DESC) AS a 
 WHERE ageband = '" . $row['ageband'] . "';";
                    
                    $resultSubReport = $userConn->query($sqlSubReport) or die($userConn->error);
                    
                    $total = count($resultSubReport - 1);
                    $total2 = $total;
                    
                    if($resultSubReport)
                    {   
                        while($rowAll = mysqli_fetch_assoc($resultSubReport))
                         {                        
                             if($total == 0)
                                    {
                                        echo "'" . $rowAll['allergen_name'] . "'";
                                    }
                                    else
                                    {
                                        echo "'" . $rowAll['allergen_name'] . "',";
                                    }
                              $total--;
                                
                             
                         }
                    }
                    
                    echo                    "],
                                data: [";
                     $resultSubReport = $userConn->query($sqlSubReport) or die($userConn->error);
                     $total2 = count($resultSubReport - 1);
                    if($resultSubReport)
                    {  
                            while($rowAll = mysqli_fetch_assoc($resultSubReport))
                         { 
                             if($total2 == 0)
                             {
                                 echo "" . $rowAll['totalInGroup'] . "";
                             }
                             else
                             {
                                 echo "" . $rowAll['totalInGroup'] . ",";
                             }
                             
                             $total2--;
                            }
                    }
                    
                    echo                "],
                                color: colors[" . $counter . "]
                                    }
                                 },";
                }
                $counter--;
            }
        }
            
            
        
    echo "    ],
        userData = [],
        AllergenData = [],
        i,
        j,
        dataLen = data.length,
        drillDataLen,
        brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        userData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            AllergenData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get()
            });
        }
    }

    // Create the chart
    $('#breakDownChart').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Allergen Spread Over Age Groups'
        },
        subtitle: {
            text: ''
        },
        yAxis: {
            title: {
                text: 'Total Number Affected by Allergen'
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            }
        },
        tooltip: {
            valueSuffix: '%'
        },
        series: [{
            name: 'Users',
            data: userData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return this.y > 5 ? this.point.name : null;
                },
                color: '#ffffff',
                distance: -30
            }
        }, {
            name: 'Allergens',
            data: AllergenData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function () {
                    // display only if larger than 1
                    return this.y > 1 ? '<b>' + this.point.name + ':</b> ' + this.y + '%' : null;
                }
            }
        }]
    });
});</script>";
}
?>