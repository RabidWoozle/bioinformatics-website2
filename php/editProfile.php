<?php

session_start();

require_once('userDB.php');

function submitEdit()
{
    require_once('userDB.php');
    $db = new CustomDatabase();
    $db_conn = $db->connect_to_db();
    
    
    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $sex = $_POST['radioOption'];
    $zip = $_POST['zip'];
    $uid = $_SESSION['userID'];
    
    $userDateOfBirth = $_SESSION['userDateOfBirth'] ;
    
    $sql = $db_conn->prepare("UPDATE user SET user_first_name =?, user_last_name = ?, user_date_of_birth = ?, user_zipcode = ?, user_sex = ? WHERE user_id = ?");
    $sql->bind_param("sssssi", $fname, $lname, $userDateOfBirth, $zip, $sex, $uid);
    $sql->execute();
    $result = $sql->get_result();
    
    
    $_SESSION['userName'] = $fname;
    $_SESSION['userLastName'] = $lname;
    $_SESSION['userDateOfBirth'] = $userDateOfBirth;
    $_SESSION['userZip'] = $zip;
    $_SESSION['userSex'] = $sex;
    
    //echo $sex;
    //echo "YAY THIS WORKS";
    header("location: ../settings.php");
}

if(isset($_POST['SaveButton']))
{
    
    submitEdit();
    
}

?>