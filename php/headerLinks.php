<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="./favicon.ico">

<title>AllerGEN</title>

<!-- Bootstrap core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/index.css" rel="stylesheet">

<!-- Custom styles for this template -->
<!--<link href="starter-template.css" rel="stylesheet">-->


<!-- JQuery Datatable Files -->
<link href="../css/jquery.dataTables.min.css" rel="stylesheet">

<script src="../js/jquery-1.11.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="./js/ie10-viewport-bug-workaround.js"></script>-->
<script src="../js/jquery.dataTables.min.js"></script>


<!-- HighCharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<!--<script src="./js/highcharts/highcharts.js"></script>
<script src="./js/highcharts//modules/exporting.js"></script>-->



<!--<script src="./js/ie-emulation-modes-warning.js"></script>-->