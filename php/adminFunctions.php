<?php



function loadFoodTable()
{
    require_once('./php/userDB.php');
    
    echo "<table id='foodTable' class='display' cellspacing='0' width='100%'>
            <thead>
            <tr>
                <th>Food</th>
                <th>Type</th>
                <th>Allergen</th>
                <th></th>
               
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Food</th>
                <th>Type</th>
                <th>Allergen</th>
                <th></th>
                
            </tr>
        </tfoot>
        <tbody>";
    
    $sql = "SELECT foo.food_id,
	   foo.food_name,
       ft.food_type_id,
       ft.food_type_name,
       group_concat(al.allergen_name separator ', ') AS allergen
FROM food foo
LEFT JOIN food_type ft ON foo.food_type_id = ft.food_type_id
LEFT JOIN food_allergen fa ON foo.food_id = fa.food_id
LEFT JOIN allergen al ON al.allergen_id = fa.allergen_id
GROUP BY foo.food_id, foo.food_name, ft.food_type_name;
";
    $result = $userConn->query($sql) or die($userConn->error);
    
    if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            {               
                    echo "<tr>
                          <td >" . $row['food_name'] . "</td>
                          <td >" . $row['food_type_name'] . "</td>
                          <td >" . $row['allergen'] . "</td>
                          <td class='text-center'>";
                    echo "<form method='post' action=''>
                        <button name='delete_food' type='submit' class='btn btn-danger btn-sm'>Delete</button> 
                        <input name='food_id' type='hidden' value='" . $row['food_id'] . "' >
                           </form>";
                      echo " </td>
                          
                          </tr> ";    
            }
        }
                 
   echo "</tbody>
        </table>";
    
   echo '<form class="form-inline" method="post" action="">
            <input name="food_name" type="text" class="form-control" required>
                <select name="food_type_id" class="form-control"> ';
     
    $sql = "SELECT 
       ft.food_type_id,
       ft.food_type_name
FROM food_type ft";
    
    $result = $userConn->query($sql) or die($userConn->error);
    if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            {               
                    echo "<option value='" . $row['food_type_id'] . "' > " . $row['food_type_name'] . " </option> ";    
            }
        }
    echo '</select>
    
    <select name="allergen_id" class="form-control">';
    
     $sql = "SELECT 
       allergen_id,
       allergen_name
FROM allergen";
    
    $result = $userConn->query($sql) or die($userConn->error);
    if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            {               
                    echo "<option value='" . $row['allergen_id'] . "' > " . $row['allergen_name'] . " </option> ";    
            }
        }
    echo '</select>';
    
    echo '<button name="add_new_food" type="submit" class="btn btn-default form-control"> Add New Food </button>
        </form>';  
}

function loadFoodTypeTable()
{
    require_once('./php/userDB.php');
    
    echo "<table id='foodTypeTable' class='display' cellspacing='0' width='100%'>
            <thead>
            <tr>
                <th>Food Type</th>                
                
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Food Type</th>                
               
            </tr>
        </tfoot>
        <tbody>";
    
    $sql = "SELECT 	food_type_name,
		      food_type_id
              FROM food_type;
";
    $result = $userConn->query($sql) or die($userConn->error);
    
    if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            {               
                    echo "<tr>
                          <td >" . $row['food_type_name'] . "</td>";
                   
                      echo "</tr> ";    
            }
        }
                 
   echo "</tbody>
        </table>";
    
   echo '<form class="form-inline" method="post" action="">
            <input name="food_type" type="text" class="form-control" required>
          <button name="add_new_food_type" type="submit" class="btn btn-default form-control"> Add New Food </button>
        </form>';  
}


function loadAllergenTable()
{
    require_once('./php/userDB.php');
    
    echo "<table id='allergenTable' class='display' cellspacing='0' width='100%'>
            <thead>
            <tr>
                <th>Allergen</th>                
                
               
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Allergen</th>                
               
            </tr>
        </tfoot>
        <tbody>";
    
    $sql = "SELECT 	allergen_name,
		              allergen_id
            FROM allergen;
";
    $result = $userConn->query($sql) or die($userConn->error);
    
    if($result)
        {
            while($row = mysqli_fetch_assoc($result))
            {               
                    echo "<tr>
                            <form method='post' action=''>
                          <td >" . $row['allergen_name'] . "</td>";
                   
                      echo " 
                          </form>
                          </tr> ";    
            }
        }
                 
   echo "</tbody>
        </table>";
    
   echo '<form class="form-inline" method="post" action="">
            <input name="allergen_name" type="text" class="form-control" required>
          <button name="add_new_allergen" type="submit" class="btn btn-default form-control"> Add New Allergen </button>
        </form>';  
}

if(isset($_POST['add_new_allergen']))
{
    $allergenName = mysqli_escape_string($_POST['allergen_name']);
    
    $sql = "INSERT INTO allergen(allergen_name) VALUES ('$allergenName');";
    
    $result = $userConn->query($sql) or die($userConn->error);
    
}

if(isset($_POST['add_new_food_type']))
{
    $foodTypeName = $_POST['food_type'];
    
    $sql = "INSERT INTO food_type(food_type_name) VALUES ('$foodTypeName');";
    
    $result = $userConn->query($sql) or die($userConn->error);
}

if(isset($_POST['add_new_food']))
{
    $foodName = $_POST['food_name'];
    $foodTypeID = $_POST['food_type_id'];
    $allergenID = $_POST['allergen_id'];
    
    $sql = "INSERT INTO food(food_name, food_type_id) VALUES ('$foodName', '$foodTypeID');";
    $result = $userConn->query($sql) or die($userConn->error);
    
    $foodID = $userConn->insert_id;
    
    $sql = "INSERT INTO food_allergen(food_id, allergen_id) VALUES ('$foodID', '$allergenID')";
    $result = $userConn->query($sql) or die($userConn->error);
}

if(isset($_POST['delete_food']))
{
    $foodID = $_POST['food_id'];
    
    $sql = "DELETE FROM food WHERE food_id = '$foodID';";
    $result = $userConn->query($sql) or die($userConn->error);
    
    $sql = "DELETE FROM food_allergen WHERE food_id = '$foodID';";
     $result = $userConn->query($sql) or die($userConn->error);
}

?>