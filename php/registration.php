<?php

//Programmer: Devon Belding
//Registration script for users

session_start();

require_once('userDB.php');
$database = new CustomDatabase();
$database_conn = $database->connect_to_db();

$fname=$_POST['fname'];
$lname=$_POST['lname'];
$email=$_POST['email'];
$age = $_POST['age'];
$sex = $_POST['radioOption'];
$zip = $_POST['zip'];
$password = $_POST['password'];


$hash = password_hash($password, PASSWORD_BCRYPT);

//$sql="INSERT INTO user(user_email, user_password, user_first_name, user_last_name)VALUES('$email', '$hash', '$fname', '$lname')";

 $sql = $database_conn->prepare("INSERT INTO user(
                user_first_name, 
                user_last_name, 
                user_email, 
                user_password, 
                user_date_of_birth, 
                user_zipcode, 
                user_sex)
                        
        VALUES(?, ?, ?, ?, ?, ?, ?)");
            
$sql->bind_param('sssssss', $fname, $lname, $email, $hash, $age, $zip, $sex);

if($sql->execute()){
    
    $sql->close();

    $query=$database_conn->prepare("SELECT * FROM user WHERE user_email = ? LIMIT 1");
    $query->bind_param('s', $email);
    $query->execute();

    $sRow = $query->get_result();

        if($sRow->num_rows > 0){
        
            while($userInfo = $sRow->fetch_assoc()) {
                
                $_SESSION['userID']= $userInfo['user_id'];
                $_SESSION['userEmail'] = $userInfo['user_email'];
                $_SESSION['userName'] = $userInfo['user_first_name'];
                $_SESSION['userLastName'] = $userInfo['user_last_name'];
                $_SESSION['userDateOfBirth'] = $userInfo['user_date_of_birth'];
                $_SESSION['userZip'] = $userInfo['user_zipcode'];                    
                $_SESSION['userSex'] = $userInfo['user_sex'];
            
            }

        $query->close();


        header("location: ../profile.php");
        //echo var_dump();
        }
    
}

else{
    
    echo "Error: could not create user, " . $userConn->error;
}

$userConn->close();


?>
