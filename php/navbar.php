
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">AllerGEN</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li <?php if (basename($_SERVER['PHP_SELF']) == 'index.php') {echo 'class="active"'; } ?> ><a href="index.php">Home</a></li>
                <?php 
                    if(isset($_SESSION['userID']))
                    { 
                        echo "<li ";
                        
                        if (basename($_SERVER['PHP_SELF']) == 'profile.php' || basename($_SERVER['PHP_SELF']) == 'settings.php') 
                        {
                            echo 'class="active"'; 
                        } 
                        
                        echo '><a href="profile.php">Profile</a></li>';
                    }
                ?>
                <li <?php if (basename($_SERVER['PHP_SELF']) == 'about.php') {echo 'class="active"'; } ?> ><a href="about.php">About</a></li>
                
                 <?php 
                        if(isset($_SESSION['userID']) && isset($_SESSION['admin']) && $_SESSION['admin'] = true)
                        {
                            echo "<li ";
                            
                            if(basename($_SERVER['PHP_SELF']) == 'admin.php')
                            {
                                echo 'class="active"';
                            }
                            
                            echo '><a href="admin.php">Admin</a></li>';
                        }
                ?>
            </ul>            
                              
            <?php 

                if(isset($_SESSION['userID']))
                { 
                    
            echo '<div class="collapse navbar-collapse" id="search-bar">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" id="list" tabindex="0" data-trigger="focus" data-toggle="popover" title="Your wishlist:" data-placement="bottom">
                            <span class="glyphicon glyphicon-list"></span>
                        </a>
                    </li>
                    <li>
                        <a href="profile.php">
                            <span class="glyphicon glyphicon-user"></span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="settings.php">Profile settings</a></li>
                            <li class="divider"></li>
                            <li><a href="./php/logout.php">Log out</a></li>
                        </ul>
                    </li>
                </ul>


                <form class="navbar-form" role="search">
                    <div class="form-group ">
                        <div class="input-group">
                            <input type="search" class="form-control " id="input-nav-searchbar" placeholder="Search...">
                            <span class="input-group-btn">
                                <!--                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>-->
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>';
                }

                else
                {
                    echo '
                    <div class="collapse navbar-collapse" id="home-page-navbar-collapse">
                    <form name="login" action="./php/login_exec.php" class="navbar-form navbar-right" method="post" role="login">
                        <div class="form-group">
                            <input name="userID" type="email" id="input-email-login" class="form-control" placeholder="E-Mail" required>
                            <div class="input-group">
                                <input name="pass" type="password" id="input-password-login" class="form-control" placeholder="Password" required>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default">Login</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopups="true" aria-expanded="false"><span class="caret"></span><span class="sr-only">No Account?</span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a id="signUpLink" href="#signup" data-toggle="modal">Sign Up</a>
                                        </li>
                                    </ul>

                                   
                                </div>

                            </div>
                        </div>
                    </form>
                </div>';
                }
            
            ?>

        </div><!--/.nav-collapse -->
    </div>
</nav>