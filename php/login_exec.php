<?php

/*
    Programmer: Devon Belding

*/
session_start();

require_once('userDB.php');

$db = new CustomDatabase();
$conn = $db->connect_to_db();

$email= mysqli_real_escape_string($conn, $_POST['userID']);

//$query="SELECT * FROM user WHERE user_email = '$email' LIMIT 1";

if($query = $conn->prepare("SELECT * FROM user WHERE user_email = ? LIMIT 1")){
$query->bind_param("s", $email);
$query->execute() or die(mysqli_error());

$sRow = $query->get_result();

while($userInfo = $sRow->fetch_assoc()) {
    $db_email = $userInfo['user_email'];
   // $salt = $userInfo['salt'];
    $ID = $userInfo['user_id'];
    $db_pass = $userInfo['user_password'];
    }


$enc_pass = $_POST['pass'];

if($email==$db_email && password_verify($enc_pass, $db_pass))
{
    $_SESSION['userID']=$ID;

    $query="SELECT u.user_id,
	   u.user_first_name,
       u.user_last_name,
       u.user_email,
       u.user_password,
       u.user_date_of_birth,
       u.user_zipcode,
       u.user_sex,
       CASE WHEN u.user_id = a.user_id 
       		THEN 1 ELSE 0 END AS userIsAdmin
FROM user u
LEFT JOIN admin a ON u.user_id = a.user_id
WHERE u.user_id = " . $ID . " LIMIT 1 ";

    $sRow = $conn->query($query) or die(mysqli_error());

    while($userInfo = $sRow->fetch_assoc()) 
    {
        $_SESSION['userEmail'] = $userInfo['user_email'];
        $_SESSION['userName'] = $userInfo['user_first_name'];
        $_SESSION['userLastName'] = $userInfo['user_last_name'];
        $_SESSION['userDateOfBirth'] = $userInfo['user_date_of_birth'];
        $_SESSION['userZip'] = $userInfo['user_zipcode'];
        $_SESSION['userSex'] = $userInfo['user_sex'];
        
        //Check if the user logging in is an admin
        if($userInfo['userIsAdmin'] == 1)
        {
            $_SESSION['admin'] = true;
        }

    }
    

    header("location: ../profile.php");
}
else{
    if($email!=$db_email){
        header("location: ../index.php?error=failedLogin_email");
    }
    else
        header("location: ../index.php?error=failedLogin_pass");

}


}
else{
    header("location: ../index.php?error=failedConn");
}

?>
