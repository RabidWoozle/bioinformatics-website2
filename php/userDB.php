<?php
class CustomDatabase {

    protected static $userConn;
    
    public function connect_to_db(){
    //login connection file
    
        if(!isset(self::$userConn)) {
            //$config = parse_ini_file("../../import_files/db_conf.ini");
            $config = parse_ini_file('db_conf.ini');
            
            $servername = "localhost";
            self::$userConn = new mysqli($servername, $config['username'], $config['password'], $config['dbname']);
        }
    
        if(self::$userConn === false) {
            //die("Connection failed: " . $userConn->connect_error);
            return false;
        }
        
        return self::$userConn;
    }
    
    
    public function query($query){
    
    $connection = $this -> connect_to_db();
    $result = $connection -> query($query);
    
    return $result;
        
    }
    
    public function select($query){
        
        $rows = array();
        $result = $this -> query($query);
        if($result === false)
        {
            return false;
        }
        while($row = $result -> fetch_assoc()){
            $rows[] = $row;
            
        }
        return $rows;
    }
    
    public function error(){
        $connection = $this -> connect_to_db();
        return $connection -> error;
        
    }
    
    public function quote($val){
        $connection = $this->connect_to_db();
        return "'" . mysqli_real_escape_string($connection, $val) . "'";
    }

    
}


?>