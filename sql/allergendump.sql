-- MySQL dump 10.13  Distrib 5.6.26, for Win32 (x86)
--
-- Host: localhost    Database: allergen
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allergen`
--

DROP TABLE IF EXISTS `allergen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allergen` (
  `aID` int(255) NOT NULL AUTO_INCREMENT,
  `allergen` varchar(255) NOT NULL,
  PRIMARY KEY (`aID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergen`
--

LOCK TABLES `allergen` WRITE;
/*!40000 ALTER TABLE `allergen` DISABLE KEYS */;
INSERT INTO `allergen` VALUES (14,'Legumin'),(15,'Profilin');
/*!40000 ALTER TABLE `allergen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `allergen_type`
--

DROP TABLE IF EXISTS `allergen_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allergen_type` (
  `Allergen_Type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Allergen_Type_Name` varchar(255) NOT NULL,
  PRIMARY KEY (`Allergen_Type_ID`),
  UNIQUE KEY `Allergen_Type_Name` (`Allergen_Type_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergen_type`
--

LOCK TABLES `allergen_type` WRITE;
/*!40000 ALTER TABLE `allergen_type` DISABLE KEYS */;
INSERT INTO `allergen_type` VALUES (2,'Dairy'),(1,'Eggs'),(5,'Fish'),(10,'Fruit'),(11,'Legume'),(3,'Peanuts'),(6,'Shellfish'),(8,'Soy'),(4,'Tree Nuts'),(9,'Vegetable'),(7,'Wheat');
/*!40000 ALTER TABLE `allergen_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `allergen_user`
--

DROP TABLE IF EXISTS `allergen_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allergen_user` (
  `uID` int(255) NOT NULL,
  `fID` int(255) NOT NULL,
  PRIMARY KEY (`uID`,`fID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergen_user`
--

LOCK TABLES `allergen_user` WRITE;
/*!40000 ALTER TABLE `allergen_user` DISABLE KEYS */;
INSERT INTO `allergen_user` VALUES (10,1),(10,2),(10,3);
/*!40000 ALTER TABLE `allergen_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_allergens`
--

DROP TABLE IF EXISTS `food_allergens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_allergens` (
  `aID` int(11) NOT NULL,
  `fID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_allergens`
--

LOCK TABLES `food_allergens` WRITE;
/*!40000 ALTER TABLE `food_allergens` DISABLE KEYS */;
INSERT INTO `food_allergens` VALUES (15,1),(14,2),(14,3);
/*!40000 ALTER TABLE `food_allergens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foods`
--

DROP TABLE IF EXISTS `foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foods` (
  `fID` int(255) NOT NULL AUTO_INCREMENT,
  `food` varchar(255) NOT NULL,
  `Allergen_Type_ID` int(11) NOT NULL,
  PRIMARY KEY (`fID`),
  UNIQUE KEY `food` (`food`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ascii;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foods`
--

LOCK TABLES `foods` WRITE;
/*!40000 ALTER TABLE `foods` DISABLE KEYS */;
INSERT INTO `foods` VALUES (1,'Broccoli',9),(2,'Peas',10),(3,'Peanuts',3);
/*!40000 ALTER TABLE `foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `uID` int(255) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UserID for accounts',
  `fname` varchar(15) DEFAULT NULL,
  `lname` varchar(30) CHARACTER SET utf32 NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` char(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`uID`),
  UNIQUE KEY `uID` (`uID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf16 COMMENT='This is a table to handle user login';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (10,'Mayce','Varacalli','mbvaraca@oakland.edu','$2y$10$HwRdHVkdNqaXaPtK3Re3iOYxPcK/SGsPgHl8QidV91lDESPnnChNq');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `uID` int(255) NOT NULL,
  `Email` varchar(50) CHARACTER SET utf32 NOT NULL,
  `FirstName` varchar(50) CHARACTER SET utf32 NOT NULL,
  `LastName` varchar(50) CHARACTER SET utf32 NOT NULL,
  `Age` int(11) NOT NULL,
  `ZIP` varchar(5) NOT NULL,
  `sex` varchar(10) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfo`
--

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` VALUES (10,10,'mbvaraca@oakland.edu','Mayce','Varacalli',22,'48451','');
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 15:55:51
