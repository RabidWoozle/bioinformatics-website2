-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 01, 2015 at 05:15 AM
-- Server version: 5.5.41-log
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `allergen`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergen`
--

CREATE TABLE IF NOT EXISTS `allergen` (
`aID` int(255) NOT NULL,
  `allergen` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `allergen`
--

INSERT INTO `allergen` (`aID`, `allergen`) VALUES
(14, 'Legumin'),
(15, 'Profilin');

-- --------------------------------------------------------

--
-- Table structure for table `allergen_type`
--

CREATE TABLE IF NOT EXISTS `allergen_type` (
`Allergen_Type_ID` int(11) NOT NULL,
  `Allergen_Type_Name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=12 ;

--
-- Dumping data for table `allergen_type`
--

INSERT INTO `allergen_type` (`Allergen_Type_ID`, `Allergen_Type_Name`) VALUES
(2, 'Dairy'),
(1, 'Eggs'),
(5, 'Fish'),
(10, 'Fruit'),
(11, 'Legume'),
(3, 'Peanuts'),
(6, 'Shellfish'),
(8, 'Soy'),
(4, 'Tree Nuts'),
(9, 'Vegetable'),
(7, 'Wheat');


CREATE TABLE IF NOT EXISTS `food_type` (
`Food_Type_ID` int(11) NOT NULL,
  `Food_Type_Name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=12 ;

--
-- Dumping data for table `allergen_type`
--

INSERT INTO `food_type` (`Food_Type_ID`, `Food_Type_Name`) VALUES
(2, 'Dairy'),
(1, 'Eggs'),
(5, 'Fish'),
(10, 'Fruit'),
(11, 'Legume'),
(3, 'Peanuts'),
(6, 'Shellfish'),
(8, 'Soy'),
(4, 'Tree Nuts'),
(9, 'Vegetable'),
(7, 'Wheat');


-- --------------------------------------------------------

--
-- Table structure for table `allergen_user`
--

CREATE TABLE IF NOT EXISTS `allergen_user` (
  `uID` int(255) NOT NULL,
  `fID` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `allergen_user`
--

INSERT INTO `allergen_user` (`uID`, `fID`) VALUES
(10, 1),
(10, 2),
(10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE IF NOT EXISTS `foods` (
`fID` int(255) NOT NULL,
  `food` varchar(255) NOT NULL,
  `Allergen_Type_ID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=4 ;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`fID`, `food`, `Allergen_Type_ID`) VALUES
(1, 'Broccoli', 9),
(2, 'Peas', 10),
(3, 'Peanuts', 3);

-- --------------------------------------------------------

--
-- Table structure for table `food_allergens`
--

CREATE TABLE IF NOT EXISTS `food_allergens` (
  `aID` int(11) NOT NULL,
  `fID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Dumping data for table `food_allergens`
--

INSERT INTO `food_allergens` (`aID`, `fID`) VALUES
(15, 1),
(14, 2),
(14, 3);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`uID` int(255) unsigned NOT NULL COMMENT 'UserID for accounts',
  `fname` varchar(15) DEFAULT NULL,
  `lname` varchar(30) CHARACTER SET utf32 NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` char(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COMMENT='This is a table to handle user login' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`uID`, `fname`, `lname`, `email`, `password`) VALUES
(10, 'Mayce', 'Varacalli', 'mbvaraca@oakland.edu', '$2y$10$HwRdHVkdNqaXaPtK3Re3iOYxPcK/SGsPgHl8QidV91lDESPnnChNq');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `uID` int(255) NOT NULL,
  `Email` varchar(50) CHARACTER SET utf32 NOT NULL,
  `FirstName` varchar(50) CHARACTER SET utf32 NOT NULL,
  `LastName` varchar(50) CHARACTER SET utf32 NOT NULL,
  `Age` int(11) NOT NULL,
  `ZIP` varchar(5) NOT NULL,
  `sex` varchar(10) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`ID`, `uID`, `Email`, `FirstName`, `LastName`, `Age`, `ZIP`) VALUES
(10, 10, 'mbvaraca@oakland.edu', 'Mayce', 'Varacalli', 22, '48451');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allergen`
--
ALTER TABLE `allergen`
 ADD PRIMARY KEY (`aID`);

--
-- Indexes for table `allergen_type`
--
ALTER TABLE `allergen_type`
 ADD PRIMARY KEY (`Allergen_Type_ID`), ADD UNIQUE KEY `Allergen_Type_Name` (`Allergen_Type_Name`);

--
-- Indexes for table `allergen_user`
--
ALTER TABLE `allergen_user`
 ADD PRIMARY KEY (`uID`,`fID`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
 ADD PRIMARY KEY (`fID`), ADD UNIQUE KEY `food` (`food`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`uID`), ADD UNIQUE KEY `uID` (`uID`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allergen`
--
ALTER TABLE `allergen`
MODIFY `aID` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `allergen_type`
--
ALTER TABLE `allergen_type`
MODIFY `Allergen_Type_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
MODIFY `fID` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `uID` int(255) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UserID for accounts',AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
