-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 02, 2015 at 06:05 AM
-- Server version: 5.5.41-log
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `allergen`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergen`
--

CREATE TABLE IF NOT EXISTS `allergen` (
`allergen_id` int(11) NOT NULL,
  `allergen_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii COMMENT='Protein names that cause an allergic reaction' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `allergen`
--

INSERT INTO `allergen` (`allergen_id`, `allergen_name`) VALUES
(1, 'Legumin'),
(2, 'Profilin');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE IF NOT EXISTS `food` (
`food_id` int(11) NOT NULL,
  `food_name` varchar(255) NOT NULL,
  `food_type_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii COMMENT='Food names and the category/ food type that they belong to' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`food_id`, `food_name`, `food_type_id`) VALUES
(1, 'Broccoli', 10),
(2, 'Peas', 4),
(3, 'Peanuts', 6);

-- --------------------------------------------------------

--
-- Table structure for table `food_allergen`
--

CREATE TABLE IF NOT EXISTS `food_allergen` (
  `food_id` int(11) NOT NULL,
  `allergen_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COMMENT='Contains food''s proteins that are an allergen, allows for a food to have more th';

--
-- Dumping data for table `food_allergen`
--

INSERT INTO `food_allergen` (`food_id`, `allergen_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `food_type`
--

CREATE TABLE IF NOT EXISTS `food_type` (
`food_type_id` int(11) NOT NULL,
  `food_type_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii COMMENT='Categories to group food into' AUTO_INCREMENT=12 ;

--
-- Dumping data for table `food_type`
--

INSERT INTO `food_type` (`food_type_id`, `food_type_name`) VALUES
(1, 'Dairy'),
(2, 'Eggs'),
(3, 'Fish'),
(4, 'Fruit'),
(5, 'Legume'),
(6, 'Peanuts'),
(7, 'Shellfish'),
(8, 'Soy'),
(9, 'Tree Nuts'),
(10, 'Vegetable'),
(11, 'Wheat');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`user_id` int(11) NOT NULL,
  `user_first_name` varchar(255) NOT NULL,
  `user_last_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_date_of_birth` varchar(10) NOT NULL,
  `user_zipcode` varchar(5) NOT NULL,
  `user_sex` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=ascii COMMENT='Contains all the user info, including login information' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `user_date_of_birth`, `user_zipcode`, `user_sex`) VALUES
(1, 'Mayce', 'Varacalli', 'mbvaraca@oakland.edu', '$2y$10$HwRdHVkdNqaXaPtK3Re3iOYxPcK/SGsPgHl8QidV91lDESPnnChNq', '09/08/1993', '48451', 'Female'),
(2, 'Devon', 'Belding', 'd.belding@live.com', '$2y$10$BvfcrZOK0ExPeozh2gswJ.xDIIi5SCbtqtwCg5SHmeBCEKB036hV.', '01/01/1992', '48309', 'Male'),
(3, 'Awesome', 'Sauce', 'awesomesauce@awesome.com', '$2y$10$Tepia6GiizDhmC1HrUMXTez7MPEQwXS4bJjPSnr35.HSbxahtNYJy', '02/02/1990', '48307', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `user_allergy`
--

CREATE TABLE IF NOT EXISTS `user_allergy` (
  `user_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COMMENT='Lists a user id and all their food allergy ids';

--
-- Dumping data for table `user_allergy`
--

INSERT INTO `user_allergy` (`user_id`, `food_id`) VALUES
(3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allergen`
--
ALTER TABLE `allergen`
 ADD PRIMARY KEY (`allergen_id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
 ADD PRIMARY KEY (`food_id`), ADD UNIQUE KEY `food_name` (`food_name`), ADD KEY `food_type_id` (`food_type_id`);

--
-- Indexes for table `food_allergen`
--
ALTER TABLE `food_allergen`
 ADD PRIMARY KEY (`food_id`,`allergen_id`), ADD KEY `FK_food_allergen_allergen_id_allergen_allergen_id` (`allergen_id`);

--
-- Indexes for table `food_type`
--
ALTER TABLE `food_type`
 ADD PRIMARY KEY (`food_type_id`), ADD UNIQUE KEY `food_type_name` (`food_type_name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `user_allergy`
--
ALTER TABLE `user_allergy`
 ADD PRIMARY KEY (`user_id`,`food_id`), ADD KEY `user_id` (`user_id`,`food_id`), ADD KEY `food_id` (`food_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allergen`
--
ALTER TABLE `allergen`
MODIFY `allergen_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
MODIFY `food_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `food_type`
--
ALTER TABLE `food_type`
MODIFY `food_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `food`
--
ALTER TABLE `food`
ADD CONSTRAINT `FK_food_food_type_id_food_type_food_type_id` FOREIGN KEY (`food_type_id`) REFERENCES `food_type` (`food_type_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `food_allergen`
--
ALTER TABLE `food_allergen`
ADD CONSTRAINT `FK_food_allergen_allergen_id_allergen_allergen_id` FOREIGN KEY (`allergen_id`) REFERENCES `allergen` (`allergen_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_food_allergen_food_id_food_food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_allergy`
--
ALTER TABLE `user_allergy`
ADD CONSTRAINT `FK_user_allergy_food_id_food_food_id` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `FK_user_allergy_user_id_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
