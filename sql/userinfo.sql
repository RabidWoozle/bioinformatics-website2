-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2015 at 04:01 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `allergen`
--

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `uID` int(255) NOT NULL,
  `Email` varchar(50) CHARACTER SET utf32 NOT NULL,
  `FirstName` varchar(50) CHARACTER SET utf32 NOT NULL,
  `LastName` varchar(50) CHARACTER SET utf32 NOT NULL,
  `Age` int(11) NOT NULL,
  `ZIP` varchar(5) NOT NULL,
  `sex` varchar(10) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`ID`, `uID`, `Email`, `FirstName`, `LastName`, `Age`, `ZIP`, `sex`) VALUES
(10, 10, 'd.belding@live.com', 'Devon', 'Belding', 23, '48309', 'Male'),
(11, 11, 'dmbeldin@oakland.edu', 'Devon', 'Belding', 23, '48309', 'Male');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
