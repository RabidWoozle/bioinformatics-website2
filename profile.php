<!DOCTYPE html>
<?php

session_start();
require_once('./php/userDB.php');

if(!isset($_SESSION['userID'])){

    header('Location: index.php');
    die();
}

$database = new CustomDatabase();
$database_connection = $database->connect_to_db();
$uid = $_SESSION['userID'];
$sqlUserAllergens = $database_connection->prepare("SELECT alle.allergen_name,
                            alle.allergen_id,
                            alle.GOA,
                            alle.PDB,
                            group_concat(foo.food_name separator ', ') AS foodAllergies
                     FROM user_allergy ua
                     LEFT JOIN food_allergen fa ON ua.food_id = fa.food_id
                     LEFT JOIN allergen alle ON alle.allergen_id = fa.allergen_id
                     LEFT JOIN food foo ON ua.food_id = foo.food_id
                     WHERE ua.user_id = ?
                     GROUP BY alle.allergen_name, alle.allergen_id");
$sqlUserAllergens->bind_param('i', $uid);

$sqlUserAllergens->execute();


$resutlUserAllergens = $sqlUserAllergens->get_result();

if($resutlUserAllergens)
{
    $sqlUserShouldAvoidFoods = "SELECT group_concat(DISTINCT (foo.food_name) separator ', ') AS foodsToAvoid
                                FROM food_allergen fa
                                LEFT JOIN food foo ON fa.food_id = foo.food_id
                                WHERE fa.allergen_id IN 
                                                (SELECT alle.allergen_id
                                                FROM user_allergy ua
                                                LEFT JOIN food_allergen fa ON ua.food_id = fa.food_id
                                                LEFT JOIN allergen alle ON alle.allergen_id = fa.allergen_id
                                                WHERE ua.user_id = " . $_SESSION['userID'] . ");";
    
    $resultUserShouldAvoidFoods = $database_connection->query($sqlUserShouldAvoidFoods) or die(mysqli_error());
}

?>
<html lang="en">
<head>
     <?php
        include("./php/headerLinks.php");
        ?>
    
</head>

<body>
<?php include("./php/navbar.php"); ?>

<div id="about" class="main-content container">

    <div  class="main-content row profile-side" >
        <div class="col-sm-2 col-md-3 col-md-offset-1 sidebar">
            <div class="row sidebar-profile">
                <div class="col-md-12">
                    <img src="img/profile.png" class="img-circle">
                    <h4 class="card-title">Welcome </h4>
                    <h5 class="card-subtitle"><?php echo $_SESSION['userName'] . " " . $_SESSION['userLastName']; ?> </h5>
                    <div class="row">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a href="settings.php">Settings</a>
                            </li>
                            <li class="list-group-item">
                                <a href="allergy.php">Add Allergies</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2 col-md-6 col-md-offset-1 sidebar">
          <div class="row sidebar-profile">
              <div class="col-md-12">
                  
             <h4 class="card-title">Allergens </h4>
                  
              <div class="row">
                 <div class="col-md-12">
                 <div id="allergens">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>
                                    Allergen
                                </th>
                                <th>
                                    Your Food Allergies
                                </th>
                                
                            </thead>
                            <tbody>
                                <?php

                                if($resutlUserAllergens)
                                {
                                    while($row = mysqli_fetch_assoc($resutlUserAllergens))
                                          {
                                              echo  '<tr>
                                                        <td>
                                                        <div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu-' . $row['allergen_name'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    ' . $row['allergen_name']. '    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu-' . $row['allergen_name'] . '">
    <li><a href="http://www.ebi.ac.uk/QuickGO/GProtein?ac=' . $row['GOA'] . '" target="_blank">Quick Go</a></li>
    <li><a href="http://www.rcsb.org/pdb/explore/explore.do?pdbId='. $row['PDB'] .'" target="_blank">PDB</a></li>
    <li><a href="http://www.ncbi.nlm.nih.gov/cdd/?term='. $row['allergen_name'] .'" target="_blank">CDD</a></li>
    <li><a href="http://www.ncbi.nlm.nih.gov/nuccore/?term='. $row['allergen_name'] .'" target="_blank">Nucleotide</a></li>
    <li><a href="http://www.ncbi.nlm.nih.gov/protein/?term='. $row['allergen_name'] .'" target="_blank">Protein</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="http://www.ncbi.nlm.nih.gov/gquery/?term='. $row['allergen_name'] .'" target="_blank">NCBI</a></li>
  </ul>
</div>
                                                        ' . "
                                                        </td>
                                                   
                                                        <td>
                                                        " . $row['foodAllergies'] . "
                                                        </td>
                                                    </tr>";
                                          } 
                                }
                               ?>
                            </tbody>
                        </table>

                    </div>
                  
                  </div>
                  
              </div>
          </div>
              <div class="col-md-12">

                  <h4 class="card-title">Foods you should avoid: </h4>

                  <div class="row">
                      <div class="col-md-12">
                          <div id="foods">
                                <p>
                                    <?php
                                        if($resultUserShouldAvoidFoods) 
                                        {
                                            while($row = mysqli_fetch_assoc($resultUserShouldAvoidFoods))
                                            {
                                                echo $row['foodsToAvoid'];
                                            }
                                        }
                                    ?>
                                </p>
                          </div>

                      </div>

                  </div>
              </div>              
              
      </div>

            <div class="row">
                    <div class="col-md-12">
                        <!-- <form method="post" action="allergy.php">
                            <button class="btn btn-primary" type="submit" name="add_allergies_btn">                           
                            Add Allergy                            
                            </button>
                        </form> -->
                    </div>              
              </div>
            
            </div>
        <div class="container-fluid">
            <div class="row">


            </div>
        </div>

    </div><!-- /.container -->

    </div>
    <footer class="page-footer text-center">
        <div class="container-fluid">
            <div class="row"><p>This tool was created for Bioinformatics at Oakland University Fall-2015 </p></div>
        </div>
    </footer>

</body>
</html>
